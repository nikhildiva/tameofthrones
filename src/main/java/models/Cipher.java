package models;

import lombok.Value;

@Value
public class Cipher {
    String message;
    int key;
}
