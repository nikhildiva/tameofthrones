package models;

import constant.Kingdom;
import lombok.Value;

@Value
public class Message {
    Kingdom kingdom;
    String messageContent;
}
