package support;

import constant.ControlConstants;
import constant.Kingdom;
import models.Message;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MessageParseSupport {
    public static final String DELIMITER = " ";
    public static final int REQUIRED_ARG_COUNT = 2;

    private MessageParseSupport() {
    }

    public static Message parseMessage(String inputMessageLine) {
        String[] splitMessage = inputMessageLine.split(DELIMITER, REQUIRED_ARG_COUNT);
        if (splitMessage.length == REQUIRED_ARG_COUNT) {
            try {
                return new Message(Enum.valueOf(Kingdom.class, splitMessage[0]), splitMessage[1]);
            } catch (IllegalArgumentException e) {
                //code to handle bad kingdom name
            }
        }
        return new Message(null, "");
    }

    private static boolean hasSupport(Message message) {
        return message.getKingdom().isAlly(message.getMessageContent());
    }

    public static List<String> getAllyList(Stream<String> inputFileStream) {
        return inputFileStream
                .map(MessageParseSupport::parseMessage)
                .filter(MessageParseSupport::hasSupport)
                .map(message -> message.getKingdom().name())
                .filter(kingdom -> !kingdom.equals(ControlConstants.RULER.name()))
                .distinct()
                .collect(Collectors.toList());
    }
}
