package utils;

import models.Cipher;

public class CipherUtils {
    private static final int ALPHA_LENGTH = 26;

    public static String decryptCeasarCipher(Cipher cipher) {

        StringBuilder decryptedString = new StringBuilder();
        String encryptedString = cipher.getMessage().toUpperCase();

        for (int index = 0; index < encryptedString.length(); ++index) {
            int currentChar = encryptedString.charAt(index);
            if (Character.isAlphabetic(currentChar)) {
                int decryptedChar = getASCIIAlpha(currentChar - cipher.getKey());
                decryptedString.append((char) decryptedChar);
            }
        }

        return decryptedString.toString();
    }

    private static int getASCIIAlpha(int asciiValue) {
        if (!Character.isAlphabetic(asciiValue)) {
            asciiValue += ALPHA_LENGTH;
        }
        return asciiValue;
    }
}
