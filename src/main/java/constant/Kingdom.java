package constant;

import models.Cipher;
import utils.CipherUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public enum Kingdom {
    SPACE("Gorilla"),
    LAND("Panda"),
    WATER("Octopus"),
    ICE("Mammoth"),
    AIR("Owl"),
    FIRE("Dragon");

    private final String emblem;

    private Map<Character, Integer> charMap;


    Kingdom(String emblem) {
        this.emblem = Objects.requireNonNull(emblem);

    }

    private Map<Character, Integer> getCharMap() {

        if (charMap != null) {
            return new HashMap<>(charMap);
        }

        int step = 1;
        charMap = new HashMap<>();
        String emblemUpper = emblem.toUpperCase();
        for (int index = 0; index < emblemUpper.length(); ++index) {
            Character currentChar = emblemUpper.charAt(index);
            if (!charMap.containsKey(currentChar)) {
                charMap.put(currentChar, step);
            } else {
                charMap.put(currentChar, charMap.get(currentChar) + step);
            }
        }
        return new HashMap<>(charMap);
    }

    public boolean isAlly(String secretMessage) {

        Cipher cipher = new Cipher(secretMessage, emblem.length());
        String message = CipherUtils.decryptCeasarCipher(cipher);

        Map<Character, Integer> charMapClone = getCharMap();

        for (int index = 0; index < message.length(); ++index) {
            Character currentChar = message.charAt(index);
            if (charMapClone.containsKey(currentChar)) {
                if (charMapClone.get(currentChar) == 1) {
                    charMapClone.remove(currentChar);
                } else {
                    charMapClone.put(currentChar, charMapClone.get(currentChar) - 1);
                }
            }
        }
        return charMapClone.size() == 0;
    }
}
