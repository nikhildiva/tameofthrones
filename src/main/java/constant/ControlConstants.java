package constant;

public class ControlConstants {

    private ControlConstants() {
    }

    public static final Kingdom RULER = Kingdom.SPACE;
    public static final int MINIMUM_ALLIES = 3;

}
