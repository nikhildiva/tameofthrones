package com.geektrust.goldencrown.entrypoint;

import constant.ControlConstants;
import support.MessageParseSupport;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import static constant.ControlConstants.MINIMUM_ALLIES;

public class Main {

    public static final String DELIMITER = " ";
    /*
        Assumptions
         1) encrypted message can be of any case and characters (including special) as no such constraints mentioned
         */
    public static void main(String[] args) {
        if (args.length == 0) {
            //code to handle fewer args
            return;
        }

        Path path = Paths.get(args[0]);
        try (Stream<String> fileStream = Files.lines(path)) {
            String output;
            List<String> allies = MessageParseSupport.getAllyList(fileStream);
            if (allies.size() >= MINIMUM_ALLIES) {
                output = ControlConstants.RULER.name() + DELIMITER + String.join(DELIMITER, allies);
            } else {
                output = "NONE";
            }
            System.out.println(output);
        } catch (IOException e) {
            System.exit(1);
        }
    }
}
