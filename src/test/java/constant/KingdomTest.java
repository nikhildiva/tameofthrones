package constant;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static constant.Kingdom.WATER;

@RunWith(JUnit4.class)
public class KingdomTest {

    //Positive
    @Test
    public void isAllyPositiveTest(){
        Assert.assertTrue(WATER.isAlly("VJAVWBZ"));
    }


    @Test
    public void isAllyCaseInsensitivity(){
        Assert.assertTrue(WATER.isAlly("vjavwbz"));
    }

    @Test
    public void isAllyMultipleOccurrenceAndOrder(){
        Assert.assertTrue(WATER.isAlly("jjvvaaabbbwwwzzzzz"));
    }

    //Negative
    @Test
    public void isAllyIgnoredMultipleOccurrence(){
        Assert.assertFalse(WATER.isAlly("vjawbz"));
    }

    @Test
    public void isAllyNegativeMissingChars(){
        Assert.assertFalse(WATER.isAlly("vjvvwbz"));
    }

    @Test
    public void isAllyNonAlpha(){
        Assert.assertTrue(WATER.isAlly("vja * !@#wbzvw"));
    }
}