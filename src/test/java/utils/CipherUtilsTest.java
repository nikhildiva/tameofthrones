package utils;

import models.Cipher;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith(JUnit4.class)
public class CipherUtilsTest {

    //Positive
    @Test
    public void decryptWithAlphaCAPString() {
        Cipher cipher = new Cipher("GHFRGHVWULQJ", 3);
        Assert.assertEquals("DECODESTRING", CipherUtils.decryptCeasarCipher(cipher));
    }

    @Test
    public void decryptWithSpaceAndSpecialChar(){
        Cipher cipher = new Cipher("GHFRGH $%^VWULQJ", 3);
        Assert.assertEquals("DECODESTRING", CipherUtils.decryptCeasarCipher(cipher));
    }

    @Test
    public void decryptWithCase(){
        Cipher cipher = new Cipher("ghfrgh $%^vwulqj", 3);
        Assert.assertEquals("DECODESTRING", CipherUtils.decryptCeasarCipher(cipher));
    }

    @Test
    public void decryptWithShiftOfMultiplesOf26(){
        Cipher cipher = new Cipher("DECODE $%^STRING", 26);
        Assert.assertEquals("DECODESTRING", CipherUtils.decryptCeasarCipher(cipher));
    }

    @Test
    public void decryptWithExhaustiveTest(){
        Cipher  cipher = new Cipher("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 5);
        Assert.assertEquals("VWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTU", CipherUtils.decryptCeasarCipher(cipher));

    }
}