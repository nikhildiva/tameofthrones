package support;

import constant.Kingdom;
import models.Message;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static support.MessageParseSupport.parseMessage;

@RunWith(JUnit4.class)
public class MessageParseSupportTest {

    private static Message emptyMessage = new Message(null, "");

    //Positive test
    @Test
    public void parseMessageWithTwoParts() {
        Message parsedMessage = parseMessage("WATER efgh");
        Assert.assertEquals(Kingdom.WATER, parsedMessage.getKingdom());
        Assert.assertEquals("efgh", parsedMessage.getMessageContent());
    }

    @Test
    public void parseMessageWithMultipleParts() {
        Message parsedMessage = parseMessage("WATER abcd efgh ijkl mnop");
        Assert.assertEquals(Kingdom.WATER, parsedMessage.getKingdom());
        Assert.assertEquals("abcd efgh ijkl mnop", parsedMessage.getMessageContent());
    }

    //Negative test
    @Test
    public void parseMessageWithOnePart() {
        Assert.assertEquals(emptyMessage, parseMessage("WATER"));
    }

    @Test
    public void parseMessageWithInvalidKingdom() {
        Assert.assertEquals(emptyMessage, parseMessage("WATR"));
    }

}