# Readme

## How to execute tests

```gradle test```

## How to create executable jar

```gradle jar```

## How to execute jar

```java -jar build/libs/geektrust.jar <path to input file>```

e.g:

```java -jar build/libs/geektrust.jar input/messages.txt```
